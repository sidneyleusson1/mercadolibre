//
//  detalleViewController.h
//  TestMercadoLibre
//
//  Created by SIDNEY LEONARDO LEUSSON ANGULO on 9/03/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface detalleViewController : UIViewController

@property (strong, nonatomic) NSDictionary *detalles_;
@property (weak, nonatomic) IBOutlet UIImageView *foto;
@property (weak, nonatomic) IBOutlet UILabel *nombre;
@property (weak, nonatomic) IBOutlet UILabel *precio;
@property (weak, nonatomic) IBOutlet UILabel *cantidad;
@property (weak, nonatomic) IBOutlet UILabel *condicion;
@property (weak, nonatomic) IBOutlet UILabel *codigo;

@end

NS_ASSUME_NONNULL_END
