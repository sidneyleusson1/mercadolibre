//
//  busquedaTableViewController.m
//  TestMercadoLibre
//
//  Created by SIDNEY LEONARDO LEUSSON ANGULO on 9/03/21.
//

#import "busquedaTableViewController.h"
#import "detalleViewController.h"


@interface busquedaTableViewController ()

@end

@implementation busquedaTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"====================================");
    NSLog(@"======== INICIA EL PROCESOE ========");
    NSLog(@"====================================");
    [self getData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
        return [ListaAtendimientos count];
}


-(void) getData{
    
    listas = nil;
   
    NSString *CATEGORY_ID = _filtro;
    NSLog(@"textos.text %@", CATEGORY_ID);
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.mercadolibre.com/sites/MCO/search?q=%@", CATEGORY_ID];
    
    NSMutableURLRequest *request3 = [[NSMutableURLRequest alloc] init];
    [request3 setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request3 setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request3 setURL:[NSURL URLWithString:targetUrl]];
    [request3 setHTTPMethod:@"GET"];
   // NSLog(@"request3 %@", request3);
    NSURLResponse *requestResponse3;
   // NSLog(@"requestResponse3: %@", requestResponse3);
    NSData *requestHandler3    = [NSURLConnection sendSynchronousRequest:request3 returningResponse:&requestResponse3 error:nil];
   // NSLog(@"requestHandler3: %@", requestHandler3);
    NSError *err3;
   // NSLog(@"err3: %@", err3);
    NSDictionary *json3        = [NSJSONSerialization JSONObjectWithData:requestHandler3 options:kNilOptions error:&err3];
    
   // NSLog(@"json3: %@", json3);
    ListaAtendimientos = [json3 objectForKey:@"results"];
    [self.tableView reloadData];
   // NSLog(@"ListaAtendimientos: %@", ListaAtendimientos);
  //  NSLog(@"d: %@", ListaAtendimientos);
    if (ListaAtendimientos != nil) {
        [self.tableView reloadData];
       
    }else{
           
         NSLog(@"No hay datos");
    }

    NSLog(@"====================================");
    NSLog(@"========FIN DEL PROCESO ============");
    NSLog(@"====================================");
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:  (NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    NSLog(@"====================================");
    NSLog(@"=SE MUESTRAN LOS DATOS EN LA TABLA==");
    NSLog(@"====================================");
    
        NSDictionary *atendimento = [ListaAtendimientos objectAtIndex:indexPath.row]; NSString *imageUrl   = [atendimento objectForKey:@"thumbnail"];
        NSLog(@"imageUrl: %@", imageUrl);
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            cell.imageView.image                    = [UIImage imageWithData:data];
        }];
       // NSLog(@"atendimento: %@", atendimento);
        cell.textLabel.text       = [atendimento objectForKey:@"title"];
        cell.detailTextLabel.text = [atendimento objectForKey:@"thumbnail_id"];
        cell.backgroundColor      = [UIColor colorWithRed: 1.00 green: 0.95 blue: 0.74 alpha: 1.00];
    
    return cell;
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"====================================");
    NSLog(@"= PASAMOS DATOS A LA VISTA DETALLE==");
    NSLog(@"====================================");
    
    if ([segue.identifier isEqualToString:@"detallado"]) {
        NSIndexPath *indexPath  = nil;
        NSDictionary *recipe    = nil;
        
        indexPath               = [self.tableView indexPathForSelectedRow];
        recipe                  = [ListaAtendimientos objectAtIndex:indexPath.row];
        
        NSLog(@"====================================");
        NSLog(@"=VISTA DETALLE PASANDOLE PARAMETRO =");
        NSLog(@"====================================");
        
        detalleViewController *destViewControllera  = segue.destinationViewController;
        destViewControllera.detalles_               = recipe;
    }
}

@end
