//
//  ViewController.m
//  TestMercadoLibre
//
//  Created by SIDNEY LEONARDO LEUSSON ANGULO on 9/03/21.
//

#import "ViewController.h"
#import "busquedaTableViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize textos;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)dismissKeyboard {
    [textos resignFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    NSLog(@"====================================");
    NSLog(@"= PASAMOS DATOS A LA VISTA BUSQUEDA==");
    NSLog(@"====================================");
    
    if ([segue.identifier isEqualToString:@"listado"]) {
        NSString *recipe                = nil;
        NSLog(@"textos.text:  %@", textos.text);
        if ([textos.text isEqualToString:@""]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Advertencia" message:@"Digite un texto para consultar." preferredStyle:UIAlertControllerStyleAlert];

            [alertController addAction:[UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
           
             }]];

           [self presentViewController:alertController animated:YES completion:nil];
            NSLog(@"No hay datos para pasar...");
        }else{
            NSString * dato             = textos.text;
            recipe                      = dato;

            busquedaTableViewController *destViewController = segue.destinationViewController;
            destViewController.filtro   = recipe;
            NSLog(@"NRuta:  %@", recipe);
        }
    }
}
@end
