//
//  detalleViewController.m
//  TestMercadoLibre
//
//  Created by SIDNEY LEONARDO LEUSSON ANGULO on 9/03/21.
//

#import "detalleViewController.h"

@interface detalleViewController ()

@end

@implementation detalleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSNumber *precio        = [NSNumber numberWithInteger:[[self.detalles_ objectForKey:@"price"] integerValue]];
    NSNumber *cantidad      = [NSNumber numberWithInteger:[[self.detalles_ objectForKey:@"available_quantity"] integerValue]];
    NSString *condicion     = [self.detalles_ objectForKey:@"condition"];
  
    NSLog(@"self.detalles_: %@", _detalles_);
    
    self.nombre.text        = [_detalles_ objectForKey:@"title"];
    self.precio.text        = [precio stringValue];
    self.cantidad.text      = [cantidad stringValue];
    self.condicion.text     = condicion;
    //self.codigo.text     = [codigo stringValue];
  
    NSString *imageUrl      = [self.detalles_ objectForKey:@"thumbnail"];
    NSLog(@"imageUrl: %@", imageUrl);
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
    self.foto.image         = [UIImage imageWithData:data];
     }];
}

@end
