//
//  busquedaTableViewController.h
//  TestMercadoLibre
//
//  Created by SIDNEY LEONARDO LEUSSON ANGULO on 9/03/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface busquedaTableViewController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate,UITableViewDelegate, UITableViewDataSource>{
    
    NSMutableArray *ListaAtendimientos;
    NSString *listas;
    NSArray *recipes;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *filtro;

@end

NS_ASSUME_NONNULL_END
