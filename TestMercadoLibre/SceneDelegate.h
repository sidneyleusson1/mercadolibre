//
//  SceneDelegate.h
//  TestMercadoLibre
//
//  Created by SIDNEY LEONARDO LEUSSON ANGULO on 9/03/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

